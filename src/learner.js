'use strict';
const csv=require('csvtojson');
const fs = require('fs');
const _ = require('underscore');
var path = require('path');
var async = require('async');
var vntk = require('./../lib/vntk');

class Learner  {
    constructor(datasetPath) {
        this.datasetPath = datasetPath || './dataset/marketing.csv';
        this.dataset = [];
        this.model = new vntk.BayesClassifier();
    }

    get datasetPath() {
        return this._datasetPath;
    }

    set datasetPath(value) {
        this._datasetPath = value;
    }

    fit(type) {
        if (type == 'csv') {
            console.log('$ reading dataset . . . ');
            csv()
                .fromFile(this.datasetPath)
                .on('json',(jsonObj)=>{
                    this.dataset.push({
                        sentence: jsonObj.sentence,
                        topic: jsonObj.topic
                    })

                    console.log('$ pushed ' + '[' + jsonObj.sentence + ']')
                })
                .on('done',(error)=>{
                    if (!error) {
                        console.log(' $ preparing data for training . . .')
                        var root = path.dirname(this.datasetPath)

                        var classifier = new vntk.BayesClassifier();
                        _.each(this.dataset, (item, idx, list) => {
                            classifier.addDocument(item.sentence, item.topic)
                        })
                        console.log('$ training the bot . . . ');
                        classifier.train()
                        console.log('$ exporting model . . . ');
                        classifier.exportModel(root + '/' + path.basename(this.datasetPath, '.csv') + '.model')
                    } else {
                        console.log("$ Error detected: " + error)
                    }
            })
        } else {
            var classifier = new vntk.BayesClassifier();

            var listDir = fs.readdirSync(this.datasetPath);

            // create a queue object with concurrency 2
            var q = async.queue(function(task, callback) {
                _.each(task.data, (item) => {
                    classifier.addDocument(item, task.dir)
                })
                callback();
            }, 2);

            // assign a callback
            q.drain = function() {
                console.log('###############################DONE###############################');
                classifier.train()
                console.log('$ exporting model . . . ');
                console.log('$ training bot completed !')
                classifier.exportModel(this.datasetPath + 'model.model')
                // console.log(classifier)
            };

            _.each(listDir, (dir) => {
                var files = fs.readdirSync(this.datasetPath + '/' + dir);

                _.each(files, (file) => {
                    var lines = _.reject(fs.readFileSync(this.datasetPath + '/' + dir + '/' + file, {encoding: 'ucs-2'}).split('\r\n'), (item) => {
                        return item == '';
                    });

                    q.push({dir: dir, file: file, data: lines}, (err) => {
                        if (!err) {
                            console.log('$ Pushed a line in ' + file + ' in [' + dir + ']');
                        } else {
                            console.log(err);
                        }
                    })
                })
            })
        }
    }

    loadModel(modelPath) {
        var data = fs.readFileSync(modelPath);
        data = JSON.parse(data);

        this.model.classFeatures = data.classFeatures
        this.model.classTotals = data.classTotals
        this.model.totalExamples = data.totalExamples
        this.model.smoothing = data.smoothing
        this.model.docs = data.docs
        this.model.features = data.features

        console.log('load model completed !');
    }

    predict(sentence) {
        return this.model.classify(sentence);
    }
}

module.exports = Learner;